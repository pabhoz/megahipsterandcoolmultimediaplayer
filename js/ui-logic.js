$(function(){
	adjustPlayer();
	$(window).resize(adjustPlayer);

	$(".mediaCatItem .title").click(function(){
		$(this).next(".catMenu").slideToggle();
	});
});

function adjustPlayer(){

	jarvisDebug("modificación del tamaño... ajustando...");
	// W

	$("#mediaHolder").css({
		width: $(window).innerWidth() - $("#mediaMenu").width() + "px"
	});

	// H

	$("#mediaMenu, #mediaHolder").css({
		height: $("body").height() - $("#actionBar").height() + "px"
	});

	$("#mediaWrapper, #playList").css({
		height: $("#mediaHolder").height() - $("#upperBar").height() + "px"
	});

}

function jarvisDebug(msg){

	var line = Math.floor((Math.random() * 4) + 1);

	switch(line){
		case 1:
			console.log("Jarvis dice: Jefe, %c"+msg+".",'background: #222; color: #bada55');
		break;
		case 2:
			console.log("Jarvis dice: Jefe, mis informes reportan %c"+msg+".",'background: #222; color: #bada55');
		break;
		case 3:
			console.log("Jarvis dice: Jefe, detecto %c"+msg+".",'background: #222; color: #bada55');
		break;
		case 4:
			console.log("Jarvis dice: Jefe, los sensores detectan %c"+msg+".",'background: #222; color: #bada55');
		break;
	}

}